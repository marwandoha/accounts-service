# Accounts Service

Account Service for accounts management

## About the project

Account Service is a REST API for accounts management

### Built With

* [Kotlin](https://kotlinlang.org/)
* [SpringBoot](https://spring.io/projects/spring-boot)
* [H2](https://www.h2database.com/html/main.html)
* [Flyway](https://flywaydb.org/)
* [SpringDoc](https://springdoc.org/)
* [Junit 5](https://junit.org/junit5/)
* [Mockk](https://mockk.io/)

## Getting Started

### Prerequisites

* Java JDK/JRE 11

### How to run the account services application

* Running the jar file
    * Go to the project root folder
    * Execute the following command

      ```shell
      mvnw spring-boot:run
      ```

* Deploying docker image (requires docker to be installed)
    * Go to the project root folder
	* Build the project using the following comment

      ```shell
      mvnw clean install
      ```

    * Build the docker image using the following command

      ```shell
      docker build -t bsf-accounts-service:latest .
      ```

    * Run the docker image using the following command

      ```shell
      docker run -p 8080:8080 bsf-accounts-service:latest
      ```

## Usage

|  Component  | Link  |
|---|---|
| Swagger UI | http://localhost:8080/swagger-ui/index.html |
| H2 Console | http://localhost:8080/h2-console<br />To connect use the following properties:<br />JDBC URL:  jdbc:h2:mem:accountsdb<br />Username: sa<br />Password: (leave it empty) |

## Contacts

* [Marwan DOHA](mailto:marwan.doha@live.com)