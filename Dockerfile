FROM openjdk:11
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} /deployments/accounts-service.jar
ENTRYPOINT ["java","-jar","/deployments/accounts-service.jar"]