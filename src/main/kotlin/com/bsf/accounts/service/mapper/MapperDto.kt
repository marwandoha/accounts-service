package com.bsf.accounts.service.mapper

import com.bsf.accounts.service.account.Account
import com.bsf.accounts.service.account.AccountDto
import com.bsf.accounts.service.transfer.*

/**
 * Class responsible of mapping an entity to its DTO representation
 */
class MapperDto {
    companion object {
        /**
         * Returns a list of accounts dtos from an account entity
         */
        fun accounts(accounts: List<Account>): List<AccountDto> =
            accounts.map { account -> account(account) };

        /**
         * Returns an account dto from an account entity
         */
        fun account(account: Account): AccountDto =
            AccountDto(
                account.id.toString(),
                account.accountNumber,
                account.balance,
                transferDetails(account.transfers)
            )

        /**
         * Returns a transfer dto from a transfer entity
         */
        fun transfer(transfer: Transfer): TransferDto =
            TransferDto(
                transfer.id.toString(), transfer.amount, (
                        getAccount(
                            transfer.transferDetails,
                            OperationType.CREDIT
                        )
                        ).balance,
                getAccount(
                    transfer.transferDetails,
                    OperationType.DEBIT
                ).balance

            )

        /**
         * Returns a transfer details dto list from a list of transfer details entities
         */
        private fun transferDetails(transfers: List<TransferDetail>?): List<TransferDetailDto>? =
            transfers?.map { transferDetail -> transferDetail(transferDetail) };

        /**
         * Returns a transfer detail dto from a transfer detail entity
         */
        private fun transferDetail(transferDetail: TransferDetail): TransferDetailDto =
            TransferDetailDto(
                transferId = transferDetail.transfer?.id.toString(),
                operationType = transferDetail
                    .operationType,
                amount = transferDetail.transfer?.amount.let { a ->
                    a
                } ?: run {
                    Double.NaN
                }
            )

        /**
         * Returns an account from a list of transfer detail entities by the operation type
         */
        private fun getAccount(transferDetails: List<TransferDetail>, operationType: OperationType): Account {
            return transferDetails.filter { transferDetail -> operationType == transferDetail.operationType }
                .map { transferDetail -> transferDetail.account }.first()
        }
    }

}