package com.bsf.accounts.service

import io.swagger.v3.oas.annotations.OpenAPIDefinition
import io.swagger.v3.oas.annotations.info.Info
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
@OpenAPIDefinition(
    info = Info(
        title = "Accounts Service", description = "Accounts management application",
        version =
        "v1"
    )
)
class AccountsServiceApplication

fun main(args: Array<String>) {
    runApplication<AccountsServiceApplication>(*args)
}
