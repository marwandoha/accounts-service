package com.bsf.accounts.service.configuration

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.domain.AuditorAware
import org.springframework.data.jpa.repository.config.EnableJpaAuditing
import java.util.*

@Configuration
@EnableJpaAuditing(auditorAwareRef = "userContextProvider")
class AuditingConfiguration {
    @Bean
    fun userContextProvider() = AuditorAware<String> {
        Optional.of("bsf_admin")
    }

}