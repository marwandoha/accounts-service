package com.bsf.accounts.service.exception

import com.bsf.accounts.service.exception.badrequest.BSFBadRequestException
import com.bsf.accounts.service.exception.notfound.BSFNotFoundException
import mu.KotlinLogging
import org.springframework.core.Ordered
import org.springframework.core.annotation.Order
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler


private val logger = KotlinLogging.logger {}

@ControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
class ControllerAdvisor {
    @ExceptionHandler
    fun notFoundException(ex: BSFNotFoundException): ResponseEntity<ExceptionEnvelope> {
        val exceptionEnvelope = ExceptionEnvelope(
            HttpStatus.NOT_FOUND.value(),
            arrayListOf(ex.message)
        )
        return handleException(ex, exceptionEnvelope, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler
    fun badRequestException(ex: BSFBadRequestException): ResponseEntity<ExceptionEnvelope> {
        val exceptionEnvelope = ExceptionEnvelope(
            HttpStatus.BAD_REQUEST.value(),
            arrayListOf(ex.message)
        )
        return handleException(ex, exceptionEnvelope, HttpStatus.BAD_REQUEST);
    }


    @ExceptionHandler
    fun handleMethodArgumentNotValid(
        ex: MethodArgumentNotValidException
    ): ResponseEntity<ExceptionEnvelope> {
        val bindingResult = ex.bindingResult
        val fieldErrors = bindingResult.fieldErrors
        val exceptionEnvelope = ExceptionEnvelope(
            HttpStatus.BAD_REQUEST.value(),
            fieldErrors.map { fieldError ->
                String.format(
                    "%s %s", fieldError.field, (fieldError.defaultMessage ?: String.format(
                        "is invalid"
                    ))
                )
            }
        )
        return handleException(ex, exceptionEnvelope, HttpStatus.BAD_REQUEST)
    }

    @ExceptionHandler
    fun exception(ex: Exception): ResponseEntity<ExceptionEnvelope> {
        val exceptionEnvelope = ExceptionEnvelope(
            HttpStatus.INTERNAL_SERVER_ERROR.value(),
            arrayListOf(ex.message ?: "Unhandled exception, please check log file for more information")
        )
        return handleException(ex, exceptionEnvelope, HttpStatus.INTERNAL_SERVER_ERROR)
    }

    private fun handleException(
        ex: Exception, exceptionEnvelope: ExceptionEnvelope, httpStatus:
        HttpStatus
    ): ResponseEntity<ExceptionEnvelope> {
        logger.error("Exception raised while processing request: ", ex)
        return ResponseEntity(exceptionEnvelope, httpStatus)
    }
}