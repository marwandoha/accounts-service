package com.bsf.accounts.service.exception.notfound

class AccountNumberNotFoundException(private val accountNumber: String) : BSFNotFoundException(
    String.format(
        "Account number %s not found", accountNumber
    )
)