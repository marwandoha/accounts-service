package com.bsf.accounts.service.exception.badrequest

open class BSFBadRequestException(override var message: String) : Exception() {
}