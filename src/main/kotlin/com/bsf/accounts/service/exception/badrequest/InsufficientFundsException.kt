package com.bsf.accounts.service.exception.badrequest

class InsufficientFundsException(var accountNumber: String) : BSFBadRequestException(
    message = String.format("Transfer failed. Account %s has insufficient funds", accountNumber)
)