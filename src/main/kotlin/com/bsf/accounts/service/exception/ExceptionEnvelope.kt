package com.bsf.accounts.service.exception

data class ExceptionEnvelope(val status: Int, val errors: List<String>)