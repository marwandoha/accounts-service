package com.bsf.accounts.service.exception.notfound

open class BSFNotFoundException(override var message: String) : Exception() {
}