package com.bsf.accounts.service.account

import com.bsf.accounts.service.exception.ExceptionEnvelope
import com.bsf.accounts.service.mapper.MapperDto
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.media.ArraySchema
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.responses.ApiResponses
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

/**
 * REST Account resource exposing operations related to accounts
 */
@RestController
@RequestMapping("/api/accounts")
@Tag(name = "Accounts", description = "Accounts management operations")
class AccountResource(
    @Autowired private val accountService: AccountService
) {
    /**
     * Returns all accounts
     */
    @GetMapping()
    @Operation(operationId = "getAll", summary = "Get All Accounts", description = "Service that returns all accounts")
    @ApiResponse(
        responseCode = "200",
        description = "Accounts successfully returned",
        content = [Content(
            mediaType = "application/json", array = ArraySchema(schema = Schema(implementation = AccountDto::class))
        )]
    )
    fun getAll() = ResponseEntity(MapperDto.accounts(accountService.findAll()), HttpStatus.OK)

    /**
     * Returns the account of the given account number
     */
    @GetMapping("/{accountNumber}")
    @Operation(
        operationId = "getByAccountNumber", summary = "Get Account By Account Number", description =
        "Service that returns an account by the account number"
    )
    @ApiResponses(
        value = [
            ApiResponse(
                responseCode = "200",
                description = "Account found",
                content = [Content(
                    mediaType = "application/json", schema = Schema(implementation = AccountDto::class)
                )]
            ),
            ApiResponse(
                responseCode = "404",
                description = "Account not found",
                content = [Content(
                    mediaType = "application/json", schema = Schema(implementation = ExceptionEnvelope::class)
                )]
            )
        ]
    )
    fun getByAccountNumber(@PathVariable(value = "accountNumber") accountNumber: String) = ResponseEntity(
        MapperDto
            .account(
                accountService
                    .findByAccountNumber(accountNumber)
            ), HttpStatus.OK
    )

}