package com.bsf.accounts.service.account

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Lock
import org.springframework.stereotype.Repository
import java.util.*
import javax.persistence.LockModeType


@Repository
interface AccountRepository : JpaRepository<Account, Long> {
    fun findAccountByAccountNumber(accountNumber: String): Optional<Account>

    @Lock(LockModeType.PESSIMISTIC_WRITE)
    fun findAccountWithLockByAccountNumber(accountNumber: String): Optional<Account>
}