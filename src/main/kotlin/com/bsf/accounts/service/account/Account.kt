package com.bsf.accounts.service.account

import com.bsf.accounts.service.persistence.BaseEntity
import com.bsf.accounts.service.transfer.TransferDetail
import org.springframework.data.jpa.domain.support.AuditingEntityListener
import javax.persistence.*

/**
 * The accounts entity
 */
@Entity
@EntityListeners(AuditingEntityListener::class)
data class Account(
    @Column(nullable = false, unique = true)
    var accountNumber: String,
    @Column(nullable = false)
    var balance: Double,
    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "account_id", updatable = false, insertable = false)
    var transfers: List<TransferDetail>? = ArrayList()
) : BaseEntity()