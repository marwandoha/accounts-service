package com.bsf.accounts.service.account

import com.bsf.accounts.service.transfer.TransferDetailDto
import io.swagger.v3.oas.annotations.media.Schema

/**
 * Account DTO used by the Accounts resource
 */
@Schema(name = "Account", description = "Object that represents an account")
data class AccountDto(
    val id: String,
    val accountNumber: String,
    val balance: Double,
    val transfers: List<TransferDetailDto>? = arrayListOf()
)