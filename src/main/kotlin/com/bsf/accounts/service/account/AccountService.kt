package com.bsf.accounts.service.account

import com.bsf.accounts.service.exception.badrequest.InsufficientFundsException
import com.bsf.accounts.service.exception.notfound.AccountNumberNotFoundException
import com.bsf.accounts.service.transfer.OperationType
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*
import javax.transaction.Transactional

@Service
class AccountService(
    @Autowired val accountRepository: AccountRepository
) {
    /**
     * Returns all accounts from the accounts repository
     */
    fun findAll(): List<Account> =
        accountRepository.findAll()

    /**
     * Returns the account of the given account number from the accounts repository
     */
    fun findByAccountNumber(accountNumber: String): Account =
        findByAccountNumber(accountNumber, accountRepository::findAccountByAccountNumber)

    /**
     * Returns the account of the given account number from the accounts repository while locking the
     * account in the database for update
     */
    fun findByAccountNumberToUpdate(accountNumber: String): Account =
        findByAccountNumber(accountNumber, accountRepository::findAccountWithLockByAccountNumber)

    /**
     * Updates the balance of the provided account based on the operation type
     * If the operation type is a CREDIT operation, money will be withdrawn from the account
     * If the operation type is a DEBIT operation, money will be deposited into the account
     * If the new account balance is negative an insufficient funds exception is thrown
     */
    @Transactional
    fun updateBalance(account: Account, amount: Double, operationType: OperationType) {
        val updatedBalance = account.balance + (amount * operationType.sign)
        if (updatedBalance < 0) {
            throw InsufficientFundsException(account.accountNumber)
        }
        account.balance = updatedBalance
        accountRepository.save(account)
    }

    /**
     * Find an account by the account number and the repository function that should be invoked
     * If the account is not found an account number not found exception is thrown
     */
    private fun findByAccountNumber(accountNumber: String, findByAccountCall: (String) -> Optional<Account>) =
        findByAccountCall(accountNumber).orElseThrow { AccountNumberNotFoundException(accountNumber) }
}