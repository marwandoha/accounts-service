package com.bsf.accounts.service.transfer

import com.bsf.accounts.service.persistence.BaseEntity
import org.springframework.data.jpa.domain.support.AuditingEntityListener
import javax.persistence.*

/**
 * The transfer entity
 */
@Entity
@EntityListeners(AuditingEntityListener::class)
data class Transfer(
    var amount: Double,
    @OneToMany(fetch = FetchType.LAZY, cascade = [CascadeType.ALL])
    @JoinColumn(name = "transfer_id", nullable = false)
    var transferDetails: List<TransferDetail>
) : BaseEntity()