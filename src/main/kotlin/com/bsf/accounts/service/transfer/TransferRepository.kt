package com.bsf.accounts.service.transfer

import org.springframework.data.jpa.repository.JpaRepository

interface TransferRepository : JpaRepository<Transfer, Long> {
}