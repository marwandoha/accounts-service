package com.bsf.accounts.service.transfer

import io.swagger.v3.oas.annotations.media.Schema
import javax.validation.constraints.Min
import javax.validation.constraints.NotBlank

@Schema(name = "TransferRequest", description = "An object that represents the transfer request")
data class TransferRequestDto(
    @field:NotBlank
    @field:Schema(description = "Account number from which the amount will be withdrawn")
    val fromAccount: String?,
    @field:Schema(description = "Account number from which the amount will be deposited")
    @field:NotBlank
    val toAccount: String?,
    @field:Min(value = 1)
    val amount: Double
)