package com.bsf.accounts.service.transfer

import com.bsf.accounts.service.account.AccountService
import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import javax.transaction.Transactional

private val logger = KotlinLogging.logger {}

@Service
class TransferService(
    @Autowired val transferRepository: TransferRepository,
    @Autowired val accountService: AccountService
) {
    /**
     * Creates a transfer based on the provided data
     * A lock is put in place on the fromAccount and the toAccount to handle concurent
     * transfers on accounts that are being currently processed
     * Returns the created transfer
     */
    @Transactional
    fun createTransfer(transferRequestDto: TransferRequestDto): Transfer {
        val fromAccount = accountService.findByAccountNumberToUpdate(transferRequestDto.fromAccount.orEmpty())
        val toAccount = accountService.findByAccountNumberToUpdate(transferRequestDto.toAccount.orEmpty())
        logger.info { "Withdrawing money from account" }
        accountService.updateBalance(fromAccount, transferRequestDto.amount, OperationType.CREDIT)
        logger.info { "Adding money to account" }
        accountService.updateBalance(toAccount, transferRequestDto.amount, OperationType.DEBIT)
        var creditTransfer = TransferDetail(OperationType.CREDIT, fromAccount)
        var debitTransfer = TransferDetail(OperationType.DEBIT, toAccount)
        logger.info { "Creating transfer" }
        return transferRepository.save(
            Transfer(
                transferRequestDto.amount, arrayListOf(
                    creditTransfer,
                    debitTransfer
                )
            )
        )
    }
}