package com.bsf.accounts.service.transfer

/**
 * Enum that represents an operation type
 * DEBIT stands for deposit
 * CREDIT stands for withdrawal
 */
enum class OperationType(val sign: Int) {
    DEBIT(1),
    CREDIT(-1)
}