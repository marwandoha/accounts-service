package com.bsf.accounts.service.transfer

import io.swagger.v3.oas.annotations.media.Schema

@Schema(name = "Transfer", description = "An object that represents a transfer")
data class TransferDto(
    val id: String,
    val amount: Double,
    val fromAccountBalance: Double,
    val toAccountBalance: Double
)