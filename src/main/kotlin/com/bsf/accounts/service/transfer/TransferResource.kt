package com.bsf.accounts.service.transfer

import com.bsf.accounts.service.exception.ExceptionEnvelope
import com.bsf.accounts.service.mapper.MapperDto
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.responses.ApiResponses
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.validation.Valid

/**
 * REST Transfer resource exposing operations related to transfers
 */
@RestController
@RequestMapping("/api/transfers")
@Tag(name = "Transfers", description = "Transfers management operations")
class TransferResource(
    @Autowired private val transferService: TransferService
) {
    /**
     * Creates a transfer based on a transfer request
     * Returns the created transfer
     */
    @PostMapping()
    @Operation(
        operationId = "createTransfer", summary = "Creates a transfer", description = "Service that creates a " +
                "transfer from two accounts"
    )
    @ApiResponses(
        value = [
            ApiResponse(
                responseCode = "201",
                description = "Transfer created",
                content = [Content(
                    mediaType = "application/json", schema = Schema(implementation = TransferDto::class)
                )]
            ),
            ApiResponse(
                responseCode = "400",
                description = "Invalid request or Insufficient funds",
                content = [Content(
                    mediaType = "application/json", schema = Schema(implementation = ExceptionEnvelope::class)
                )]
            ),
            ApiResponse(
                responseCode = "404",
                description = "Account not found",
                content = [Content(
                    mediaType = "application/json", schema = Schema(implementation = ExceptionEnvelope::class)
                )]
            )
        ]
    )
    fun createTransfer(@Valid @RequestBody transferRequest: TransferRequestDto) = ResponseEntity(
        MapperDto.transfer(
            transferService
                .createTransfer(transferRequest)
        ), HttpStatus.CREATED
    )
}