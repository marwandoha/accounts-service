package com.bsf.accounts.service.transfer

import io.swagger.v3.oas.annotations.media.Schema

@Schema(name = "TransferDetail", description = "An object that represents a transfer detail")
data class TransferDetailDto(
    val transferId: String,
    val operationType: OperationType,
    val amount: Double
)