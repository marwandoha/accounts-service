package com.bsf.accounts.service.transfer

import com.bsf.accounts.service.account.Account
import com.bsf.accounts.service.persistence.BaseEntity
import org.springframework.data.jpa.domain.support.AuditingEntityListener
import javax.persistence.*

/**
 * The transfer detail entity
 * This holds the information of the accounts involved in a transfer
 */
@Entity
@EntityListeners(AuditingEntityListener::class)
data class TransferDetail(
    @Enumerated(EnumType.STRING)
    val operationType: OperationType,
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "account_id", nullable = false)
    val account: Account,
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "transfer_id", nullable = false, updatable = false, insertable = false)
    val transfer: Transfer? = null
) : BaseEntity() {
}