package com.bsf.accounts.service.persistence

import org.springframework.data.annotation.CreatedBy
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedBy
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener
import java.time.LocalDateTime
import javax.persistence.*

/**
 * Base entity that holds all common auditing properties that should be inherited by all entities
 */
@MappedSuperclass
@EntityListeners(AuditingEntityListener::class)
abstract class BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    open var id: Long? = null

    @CreatedBy
    @Column(nullable = false, updatable = false)
    open var createdBy: String? = null

    @CreatedDate
    @Column(nullable = false, updatable = false)
    open var createdDate: LocalDateTime? = null

    @LastModifiedBy
    open var modifiedBy: String? = null

    @LastModifiedDate
    open var modifiedDate: LocalDateTime? = null

}


