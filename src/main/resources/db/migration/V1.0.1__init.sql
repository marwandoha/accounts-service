insert into ACCOUNT (account_number, balance, created_date, created_by)
values ('431222', '5000',
        CURRENT_TIMESTAMP,
        'bsf_admin');
insert into ACCOUNT (account_number, balance, created_date, created_by)
values ('431225', '10000', CURRENT_TIMESTAMP, 'bsf_admin');
insert into ACCOUNT (account_number, balance, created_date, created_by)
values ('593111', '3433', CURRENT_TIMESTAMP, 'bsf_admin');
insert into ACCOUNT (account_number, balance, created_date, created_by)
values ('654777', '6400', CURRENT_TIMESTAMP, 'bsf_admin');
insert into ACCOUNT (account_number, balance, created_date, created_by)
values ('821552', '1200', CURRENT_TIMESTAMP, 'bsf_admin');
insert into ACCOUNT (account_number, balance, created_date, created_by)
values ('991177', '3000', CURRENT_TIMESTAMP, 'bsf_admin');