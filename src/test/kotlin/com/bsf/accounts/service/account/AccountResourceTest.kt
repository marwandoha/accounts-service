package com.bsf.accounts.service.account

import com.bsf.accounts.service.exception.notfound.AccountNumberNotFoundException
import com.bsf.accounts.service.fixtures.AccountFixture.Companion.anAccount
import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import io.mockk.verify
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertAll
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.http.MediaType
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

private const val RESOURCE_URL = "/api/accounts"

@WebMvcTest(AccountResource::class)
@ActiveProfiles("test")
internal class AccountResourceTest() {
    @Autowired
    lateinit var mockMvc: MockMvc

    @MockkBean
    private lateinit var accountService: AccountService

    @Test
    fun `getAllAccounts`() {
        every { accountService.findAll() } returns arrayListOf(anAccount(), anAccount());

        mockMvc.perform(get(RESOURCE_URL))
            .andExpect(status().isOk)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON));

        assertAll(
            { verify(exactly = 1) { accountService.findAll() } }
        )
    }

    @Test
    fun `getAccountByAccountNumber`() {
        val account = anAccount()
        every { accountService.findByAccountNumber(account.accountNumber) } returns account;

        mockMvc.perform(get(String.format("%s/%s", RESOURCE_URL, account.accountNumber)))
            .andExpect(status().isOk)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON));

        assertAll(
            { verify(exactly = 1) { accountService.findByAccountNumber(account.accountNumber) } }
        )
    }

    @Test
    fun `getAccountByAccountNumberNotFound`() {
        val accountNumber = "123"
        every { accountService.findByAccountNumber(accountNumber) } throws AccountNumberNotFoundException(accountNumber);

        mockMvc.perform(get(String.format("%s/%s", RESOURCE_URL, accountNumber)))
            .andExpect(status().isNotFound)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON));

        assertAll(
            { verify(exactly = 1) { accountService.findByAccountNumber(accountNumber) } }
        )
    }
}