package com.bsf.accounts.service.account

import com.bsf.accounts.service.exception.badrequest.InsufficientFundsException
import com.bsf.accounts.service.exception.notfound.AccountNumberNotFoundException
import com.bsf.accounts.service.fixtures.AccountFixture
import com.bsf.accounts.service.fixtures.AccountFixture.Companion.anAccount
import com.bsf.accounts.service.transfer.OperationType
import io.mockk.every
import io.mockk.impl.annotations.InjectMockKs
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.junit5.MockKExtension
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.api.extension.ExtendWith
import java.util.*


@ExtendWith(MockKExtension::class)
internal class AccountServiceTest {
    @RelaxedMockK
    lateinit var accountRepository: AccountRepository

    @InjectMockKs
    lateinit var accountService: AccountService

    @Test
    fun `return all accounts`() {
        val accounts = arrayListOf(AccountFixture.anAccount(), AccountFixture.anAccount())
        every { accountRepository.findAll() }.returns(accounts)
        assertEquals(accountService.findAll(), accounts)
    }

    @Test
    fun `return account by account number`() {
        val account = anAccount()
        every { accountRepository.findAccountByAccountNumber(account.accountNumber) }.returns(Optional.of(account))
        assertEquals(accountService.findByAccountNumber(account.accountNumber), account)
    }

    @Test
    fun `account number not found`() {
        every { accountRepository.findAccountByAccountNumber(any()) }.returns(Optional.empty())
        assertThrows<AccountNumberNotFoundException> { accountService.findByAccountNumber("123") }
    }

    @Test
    fun `return account by account number for update`() {
        val account = anAccount()
        every { accountRepository.findAccountWithLockByAccountNumber(account.accountNumber) }.returns(
            Optional.of(
                account
            )
        )
        assertEquals(accountService.findByAccountNumberToUpdate(account.accountNumber), account)
    }

    @Test
    fun `account number for update not found`() {
        every { accountRepository.findAccountWithLockByAccountNumber(any()) }.returns(Optional.empty())
        assertThrows<AccountNumberNotFoundException> { accountService.findByAccountNumberToUpdate("123") }
    }

    @Test
    fun `withdrawing money from account`() {
        var account = anAccount(balance = 1000.0)
        every { accountRepository.save(any()) }.returns(account)
        every { accountRepository.findAccountByAccountNumber(account.accountNumber) }.returns(Optional.of(account))

        accountService.updateBalance(account, 100.0, OperationType.CREDIT)

        assertEquals(account.balance, 900.0)
    }

    @Test
    fun `depositing money to account`() {
        var account = anAccount(balance = 1000.0)
        every { accountRepository.save(any()) }.returns(account)
        every { accountRepository.findAccountByAccountNumber(account.accountNumber) }.returns(Optional.of(account))

        accountService.updateBalance(account, 100.0, OperationType.DEBIT)

        assertEquals(account.balance, 1100.0)
    }

    @Test
    fun `insufficient funds for withdrawal`() {
        var account = anAccount(balance = 1000.0)
        every { accountRepository.save(any()) }.returns(account)

        assertThrows<InsufficientFundsException> { accountService.updateBalance(account, 2000.0, OperationType.CREDIT) }
    }
}
