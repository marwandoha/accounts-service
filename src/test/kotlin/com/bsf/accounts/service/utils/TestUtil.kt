package com.bsf.accounts.service.utils

import kotlin.random.Random

object TestUtil {
    private val alphabet: List<Char> = ('a'..'z') + ('A'..'Z') + ('0'..'9')

    //fun anAccount(): Account = Account(randomString(), randomDouble());

    //fun anAccountWithBalance(balance: Double): Account = Account(randomString(), balance)

    //fun aListOfAccounts(): List<Account> = arrayListOf(anAccount(), anAccount(), anAccount())

//    fun aTransfer(amount: Double, fromAccount: Account, toAccount: Account): Transfer {
//        return Transfer(
//            amount,
//            arrayListOf(
//                TransferDetail(OperationType.CREDIT, fromAccount),
//                TransferDetail(OperationType.DEBIT, toAccount)
//            )
//        )
//    }

//    fun aTransferRequest(fromAccount: String, toAccount: String, amount: Double): TransferRequestDto =
//        TransferRequestDto(
//            fromAccount,
//            toAccount, amount
//        )

    fun randomString(): String {
        return List(10) { alphabet.random() }.joinToString("")
    }

    fun randomDouble(): Double {
        return Random.nextDouble(1000.0, 10000.0);
    }

    fun randomId(): Long {
        return Random.nextLong()
    }
}