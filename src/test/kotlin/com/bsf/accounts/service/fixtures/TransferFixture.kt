package com.bsf.accounts.service.fixtures

import com.bsf.accounts.service.transfer.Transfer
import com.bsf.accounts.service.transfer.TransferDetail
import com.bsf.accounts.service.utils.TestUtil.randomDouble
import com.bsf.accounts.service.utils.TestUtil.randomId

class TransferFixture {
    companion object {
        fun aTransfer(
            id: Long = randomId(),
            amount: Double = randomDouble(),
            transferDetails: List<TransferDetail> = arrayListOf()
        ): Transfer {
            val transfer = Transfer(amount = amount, transferDetails = transferDetails)
            transfer.id = id
            return transfer
        }
    }
}