package com.bsf.accounts.service.fixtures

import com.bsf.accounts.service.transfer.TransferRequestDto
import com.bsf.accounts.service.utils.TestUtil.randomDouble

class TransferRequestDtoFixture {
    companion object {
        fun aTransferRequestDto(
            fromAccount: String,
            toAccount: String,
            amount: Double = randomDouble()
        ): TransferRequestDto {
            return TransferRequestDto(fromAccount = fromAccount, toAccount = toAccount, amount = amount)
        }
    }
}