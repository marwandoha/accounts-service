package com.bsf.accounts.service.fixtures

import com.bsf.accounts.service.transfer.OperationType
import com.bsf.accounts.service.transfer.TransferDetailDto
import com.bsf.accounts.service.utils.TestUtil.randomDouble
import com.bsf.accounts.service.utils.TestUtil.randomId

class TransferDetailDtoFixture {
    companion object {
        fun aTransferDetailDto(
            operationType: OperationType,
            transferId: String = randomId().toString(),
            amount: Double = randomDouble()
        ): TransferDetailDto {
            return TransferDetailDto(operationType = operationType, transferId = transferId, amount = amount)
        }
    }
}