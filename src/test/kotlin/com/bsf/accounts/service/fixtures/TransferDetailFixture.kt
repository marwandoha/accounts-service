package com.bsf.accounts.service.fixtures

import com.bsf.accounts.service.account.Account
import com.bsf.accounts.service.transfer.OperationType
import com.bsf.accounts.service.transfer.Transfer
import com.bsf.accounts.service.transfer.TransferDetail

class TransferDetailFixture {
    companion object {
        fun aTransferDetail(
            operationType: OperationType,
            account: Account,
            transfer: Transfer? = null
        ): TransferDetail {
            return TransferDetail(operationType = operationType, account = account, transfer = transfer)
        }
    }
}