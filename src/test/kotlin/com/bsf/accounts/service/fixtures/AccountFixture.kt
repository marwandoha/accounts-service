package com.bsf.accounts.service.fixtures

import com.bsf.accounts.service.account.Account
import com.bsf.accounts.service.transfer.TransferDetail
import com.bsf.accounts.service.utils.TestUtil.randomDouble
import com.bsf.accounts.service.utils.TestUtil.randomId
import com.bsf.accounts.service.utils.TestUtil.randomString

class AccountFixture {
    companion object {
        fun anAccount(
            id: Long = randomId(),
            accountNumber: String = randomString(),
            balance: Double = randomDouble(),
            transfers: List<TransferDetail> = emptyList()
        ): Account {
            val account = Account(accountNumber = accountNumber, balance = balance, transfers = transfers)
            account.id = id
            return account
        }
    }
}