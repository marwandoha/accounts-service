package com.bsf.accounts.service.fixtures

import com.bsf.accounts.service.transfer.TransferDto
import com.bsf.accounts.service.utils.TestUtil.randomId

class TransferDtoFixture {
    companion object {
        fun aTransferDto(
            id: String = randomId().toString(),
            amount: Double,
            fromAccountBalance: Double,
            toAccountBalance: Double
        ): TransferDto {
            return TransferDto(
                id = id,
                amount = amount,
                fromAccountBalance = fromAccountBalance,
                toAccountBalance = toAccountBalance
            )
        }
    }
}