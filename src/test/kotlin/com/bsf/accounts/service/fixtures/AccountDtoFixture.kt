package com.bsf.accounts.service.fixtures

import com.bsf.accounts.service.account.AccountDto
import com.bsf.accounts.service.transfer.TransferDetailDto
import com.bsf.accounts.service.utils.TestUtil.randomDouble
import com.bsf.accounts.service.utils.TestUtil.randomId
import com.bsf.accounts.service.utils.TestUtil.randomString

class AccountDtoFixture {
    companion object {
        fun anAccountDto(
            id: String = randomId().toString(),
            accountNumber: String = randomString(),
            balance: Double = randomDouble(),
            transfers: List<TransferDetailDto> = emptyList()
        ): AccountDto {
            return AccountDto(id = id, accountNumber = accountNumber, balance = balance, transfers = transfers)
        }
    }
}