package com.bsf.accounts.service.mapper

import com.bsf.accounts.service.fixtures.AccountDtoFixture.Companion.anAccountDto
import com.bsf.accounts.service.fixtures.AccountFixture.Companion.anAccount
import com.bsf.accounts.service.fixtures.TransferDetailDtoFixture.Companion.aTransferDetailDto
import com.bsf.accounts.service.fixtures.TransferDetailFixture.Companion.aTransferDetail
import com.bsf.accounts.service.fixtures.TransferDtoFixture.Companion.aTransferDto
import com.bsf.accounts.service.fixtures.TransferFixture.Companion.aTransfer
import com.bsf.accounts.service.transfer.OperationType
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class MapperDtoTest {
    @Test
    fun `account mapper`() {
        val account = anAccount()
        val transfer = aTransfer()
        val transferDetail = aTransferDetail(
            transfer = transfer, account = account, operationType = OperationType
                .CREDIT
        )
        account.transfers = arrayListOf(transferDetail)

        val expectedTransferDetail = aTransferDetailDto(
            operationType = transferDetail.operationType, transferId =
            transfer.id.toString(), amount = transfer.amount
        )
        val expectedDto = anAccountDto(
            id = account.id.toString(), accountNumber = account.accountNumber,
            balance = account.balance, transfers = arrayListOf(expectedTransferDetail)
        )

        assertEquals(MapperDto.account(account), expectedDto)

        // testing conversion to list of accounts
        assertEquals((MapperDto.accounts(arrayListOf(account))), arrayListOf(expectedDto))
    }

    @Test
    fun `transfer mapper`() {
        val creditAccount = anAccount()
        val debitAccount = anAccount()
        val transfer = aTransfer()
        val transferCreditDetail = aTransferDetail(
            transfer = transfer, account = creditAccount, operationType = OperationType
                .CREDIT
        )
        val transferDebitDetail = aTransferDetail(
            transfer = transfer, account = debitAccount, operationType = OperationType
                .DEBIT
        )
        transfer.transferDetails = arrayListOf(transferCreditDetail, transferDebitDetail)

        val expectedDto = aTransferDto(
            id = transfer.id.toString(), amount = transfer.amount, fromAccountBalance =
            creditAccount.balance, debitAccount.balance
        )

        assertEquals(MapperDto.transfer(transfer), expectedDto)
    }
}