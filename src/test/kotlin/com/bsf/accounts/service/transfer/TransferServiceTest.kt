package com.bsf.accounts.service.transfer

import com.bsf.accounts.service.account.AccountService
import com.bsf.accounts.service.fixtures.AccountFixture.Companion.anAccount
import com.bsf.accounts.service.fixtures.TransferDetailFixture.Companion.aTransferDetail
import com.bsf.accounts.service.fixtures.TransferFixture.Companion.aTransfer
import com.bsf.accounts.service.fixtures.TransferRequestDtoFixture.Companion.aTransferRequestDto
import io.mockk.every
import io.mockk.impl.annotations.InjectMockKs
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.junit5.MockKExtension
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(MockKExtension::class)
internal class TransferServiceTest {
    @RelaxedMockK
    lateinit var transferRepository: TransferRepository

    @RelaxedMockK
    lateinit var accountService: AccountService

    @InjectMockKs
    lateinit var transferService: TransferService

    @Test
    fun `create transfer`() {
        val withdrawalAccount = anAccount(balance = 2000.0)
        val depositAccount = anAccount(balance = 500.0)
        val transfer = aTransfer(
            amount = 1000.0, transferDetails = arrayListOf(
                aTransferDetail(
                    operationType = OperationType.CREDIT,
                    account =
                    withdrawalAccount
                ), aTransferDetail(
                    operationType = OperationType.DEBIT,
                    account =
                    depositAccount
                )
            )
        )
        val transferRequest = aTransferRequestDto(
            fromAccount = withdrawalAccount.accountNumber, toAccount = depositAccount
                .accountNumber, amount = transfer.amount
        )
        every { accountService.findByAccountNumberToUpdate(withdrawalAccount.accountNumber) }.returns(withdrawalAccount)
        every { accountService.findByAccountNumberToUpdate(depositAccount.accountNumber) }.returns(depositAccount)
        every {
            transferRepository.save(transfer)
        }.returns(transfer)

        assertEquals(transferService.createTransfer(transferRequest), transfer)
    }
}