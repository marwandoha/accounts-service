package com.bsf.accounts.service.transfer

import com.bsf.accounts.service.exception.badrequest.InsufficientFundsException
import com.bsf.accounts.service.exception.notfound.AccountNumberNotFoundException
import com.bsf.accounts.service.fixtures.AccountFixture.Companion.anAccount
import com.bsf.accounts.service.fixtures.TransferDetailFixture.Companion.aTransferDetail
import com.bsf.accounts.service.fixtures.TransferFixture.Companion.aTransfer
import com.bsf.accounts.service.fixtures.TransferRequestDtoFixture.Companion.aTransferRequestDto
import com.fasterxml.jackson.databind.ObjectMapper
import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import io.mockk.verify
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertAll
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.http.MediaType
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers

private const val RESOURCE_URL = "/api/transfers"

@WebMvcTest(TransferResource::class)
@ActiveProfiles("test")
internal class TransferResourceTest {
    @Autowired
    lateinit var mockMvc: MockMvc

    @MockkBean
    private lateinit var transferService: TransferService

    private val mapper = ObjectMapper()

    @Test
    fun `create transfer`() {
        val fromAccount = anAccount()
        val toAccount = anAccount()
        val transferRequest =
            aTransferRequestDto(fromAccount = fromAccount.accountNumber, toAccount = toAccount.accountNumber)
        every { transferService.createTransfer(transferRequest) } returns aTransfer(
            transferDetails = arrayListOf
                (
                aTransferDetail(operationType = OperationType.CREDIT, account = fromAccount), aTransferDetail
                (operationType = OperationType.DEBIT, account = toAccount)
            )
        );

        mockMvc.perform(
            MockMvcRequestBuilders.post(RESOURCE_URL).contentType(MediaType.APPLICATION_JSON).content
                (mapper.writeValueAsString(transferRequest))
        )
            .andExpect(MockMvcResultMatchers.status().isCreated)
            .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON));

        assertAll(
            { verify(exactly = 1) { transferService.createTransfer(transferRequest) } }
        )
    }

    @Test
    fun `create transfer without amount`() {
        val fromAccount = anAccount()
        val toAccount = anAccount()
        val transferRequest =
            aTransferRequestDto(
                fromAccount = fromAccount.accountNumber, toAccount = toAccount.accountNumber, amount
                = 0.0
            )

        mockMvc.perform(
            MockMvcRequestBuilders.post(RESOURCE_URL).contentType(MediaType.APPLICATION_JSON).content
                (mapper.writeValueAsString(transferRequest))
        )
            .andExpect(MockMvcResultMatchers.status().isBadRequest)
            .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON));

        assertAll(
            { verify(exactly = 0) { transferService.createTransfer(transferRequest) } }
        )
    }

    @Test
    fun `create transfer without from account`() {
        val toAccount = anAccount()
        val transferRequest =
            aTransferRequestDto(
                fromAccount = "", toAccount = toAccount.accountNumber
            )

        mockMvc.perform(
            MockMvcRequestBuilders.post(RESOURCE_URL).contentType(MediaType.APPLICATION_JSON).content
                (mapper.writeValueAsString(transferRequest))
        )
            .andExpect(MockMvcResultMatchers.status().isBadRequest)
            .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON));

        assertAll(
            { verify(exactly = 0) { transferService.createTransfer(transferRequest) } }
        )
    }

    @Test
    fun `create transfer without to account`() {
        val fromAccount = anAccount()
        val transferRequest =
            aTransferRequestDto(
                fromAccount = fromAccount.accountNumber, toAccount = ""
            )

        mockMvc.perform(
            MockMvcRequestBuilders.post(RESOURCE_URL).contentType(MediaType.APPLICATION_JSON).content
                (mapper.writeValueAsString(transferRequest))
        )
            .andExpect(MockMvcResultMatchers.status().isBadRequest)
            .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON));

        assertAll(
            { verify(exactly = 0) { transferService.createTransfer(transferRequest) } }
        )
    }

    @Test
    fun `create transfer with insufficient funds`() {
        val fromAccount = anAccount()
        val toAccount = anAccount()
        val transferRequest =
            aTransferRequestDto(fromAccount = fromAccount.accountNumber, toAccount = toAccount.accountNumber)
        every { transferService.createTransfer(transferRequest) } throws InsufficientFundsException(fromAccount.accountNumber);

        mockMvc.perform(
            MockMvcRequestBuilders.post(RESOURCE_URL).contentType(MediaType.APPLICATION_JSON).content
                (mapper.writeValueAsString(transferRequest))
        )
            .andExpect(MockMvcResultMatchers.status().isBadRequest)
            .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON));

        assertAll(
            { verify(exactly = 1) { transferService.createTransfer(transferRequest) } }
        )
    }

    @Test
    fun `create transfer with account not found`() {
        val fromAccount = anAccount()
        val toAccount = anAccount()
        val transferRequest =
            aTransferRequestDto(fromAccount = fromAccount.accountNumber, toAccount = toAccount.accountNumber)
        every { transferService.createTransfer(transferRequest) } throws AccountNumberNotFoundException(
            toAccount
                .accountNumber
        );

        mockMvc.perform(
            MockMvcRequestBuilders.post(RESOURCE_URL).contentType(MediaType.APPLICATION_JSON).content
                (mapper.writeValueAsString(transferRequest))
        )
            .andExpect(MockMvcResultMatchers.status().isNotFound)
            .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON));

        assertAll(
            { verify(exactly = 1) { transferService.createTransfer(transferRequest) } }
        )
    }
}